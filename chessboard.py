__author__ = 'hyst329'
from enum import Enum


class Piece(Enum):
    Empty = 0
    WhiteKing = 1
    WhiteQueen = 2
    WhiteBishop = 3
    WhiteKnight = 4
    WhiteRook = 5
    WhitePawn = 6
    BlackKing = -1
    BlackQueen = -2
    BlackBishop = -3
    BlackKnight = -4
    BlackRook = -5
    BlackPawn = -6


class Chessboard:
    def __init__(self):
        self.board = [[]] * 8
        self.white_to_move = True
        for i in range(8):
            self.board[i] = ([Piece.Empty] * 8).copy()
        self.startingPosition()
        self.en_passant = None
        self.castling = set("KQkq")
        self.move_number = 0

    def startingPosition(self):
        self['e1'] = Piece.WhiteKing
        self['d1'] = Piece.WhiteQueen
        self['c1'] = self['f1'] = Piece.WhiteBishop
        self['b1'] = self['g1'] = Piece.WhiteKnight
        self['a1'] = self['h1'] = Piece.WhiteRook
        self['e8'] = Piece.BlackKing
        self['d8'] = Piece.BlackQueen
        self['c8'] = self['f8'] = Piece.BlackBishop
        self['b8'] = self['g8'] = Piece.BlackKnight
        self['a8'] = self['h8'] = Piece.BlackRook
        for i in range(8):
            self[chr(ord('a') + i) + '2'] = Piece.WhitePawn
            self[chr(ord('a') + i) + '7'] = Piece.BlackPawn
        self.white_to_move = True

    def parseFEN(self, fen):
        for i in range(8):
            self.board[i] = ([Piece.Empty] * 8).copy()
        pieces, color, castle, en_passant, clock, move = fen.split(' ')
        self.white_to_move = "bw".index(color)
        self.en_passant = en_passant if en_passant != "-" else None
        self.move_number = int(move) - 1
        self.castling = set(castle) if castle != '-' else set()
        ranks = pieces.split('/')
        piece_names = {'K': Piece.WhiteKing, 'Q': Piece.WhiteQueen, 'R': Piece.WhiteRook,
                       'B': Piece.WhiteBishop, 'N': Piece.WhiteKnight, 'P': Piece.WhitePawn,
                       'k': Piece.BlackKing, 'q': Piece.BlackQueen, 'r': Piece.BlackRook,
                       'b': Piece.BlackBishop, 'n': Piece.BlackKnight, 'p': Piece.BlackPawn}
        for r in range(8):
            rank = ranks[r]
            rindex = str(8 - r)
            findex = 1
            for c in rank:
                if c in piece_names:
                    # print(c, chr(ord('a') + findex - 1) + rindex)
                    self[chr(ord('a') + findex - 1) + rindex] = piece_names[c]
                    findex += 1
                else:
                    findex += int(c)

    def __getitem__(self, item):
        i = ord(item[0]) - ord('a')
        j = ord(item[1]) - ord('1')
        return self.board[i][j]

    def __setitem__(self, key, value):
        i = ord(key[0]) - ord('a')
        j = ord(key[1]) - ord('1')
        self.board[i][j] = value

    def isWhitePiece(self, square):
        return self[square].value > 0

    def isBlackPiece(self, square):
        return self[square].value < 0

    def findKing(self, white):
        king = Piece.WhiteKing if white else Piece.BlackKing
        for i in range(8):
            for j in range(8):
                if self.board[i][j] == king:
                    return chr(ord('a') + i) + chr(ord('1') + j)

    def move(self, move):
        if not 4 <= len(move) <= 5:
            return
        start, dest = move[0:2], move[2:4]
        if start == "e1" and ("K" in self.castling or "Q" in self.castling):
            if "K" in self.castling:
                self.castling.remove("K")
            if "Q" in self.castling:
                self.castling.remove("Q")
            if dest == "g1":
                self["f1"] = Piece.WhiteRook
                self["h1"] = Piece.Empty
            if dest == "c1":
                self["d1"] = Piece.WhiteRook
                self["a1"] = Piece.Empty
        if start == "e8" and ("k" in self.castling or "q" in self.castling):
            if "k" in self.castling:
                self.castling.remove("k")
            if "q" in self.castling:
                self.castling.remove("q")
            if dest == "g8":
                self["f8"] = Piece.BlackRook
                self["h8"] = Piece.Empty
            if dest == "c8":
                self["d8"] = Piece.BlackRook
                self["a8"] = Piece.Empty
        if "h1" in (start, dest) and "K" in self.castling:
            self.castling.remove("K")
        if "a1" in (start, dest) and "Q" in self.castling:
            self.castling.remove("Q")
        if "h8" in (start, dest) and "k" in self.castling:
            self.castling.remove("k")
        if "a8" in (start, dest) and "q" in self.castling:
            self.castling.remove("q")

        en_passant_capture = ((self[start] in (Piece.BlackPawn, Piece.WhitePawn))
                              and (dest == self.en_passant))

        if self[start] == Piece.WhitePawn and move[1] == '2' and move[3] == '4':
            self.en_passant = move[0] + '3'
        elif self[start] == Piece.BlackPawn and move[1] == '7' and move[3] == '5':
            self.en_passant = move[0] + '6'
        else:
            self.en_passant = None

        capture = self[dest] != Piece.Empty

        self[dest] = self[start]
        self[start] = Piece.Empty

        if en_passant_capture:
            pawn = move[2] + chr(ord(move[3]) + (1, -1)[self.white_to_move])
            self[pawn] = Piece.Empty
            capture = True

        if self.white_to_move:
            self.move_number += 1
        if len(move) == 5:
            piece = move[4]
            value = Piece("__qbnr_".index(piece) * (-1, 1)[self.white_to_move])
            self[dest] = value
            # print(value)
        self.white_to_move = not self.white_to_move

        return capture