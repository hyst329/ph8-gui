import time
from chessboard import Piece
from copy import deepcopy

__author__ = 'hyst329'


class MoveGenerator:
    @staticmethod
    def toSquare(i, j):
        return chr(i + ord('a')) + chr(j + ord('1'))

    @staticmethod
    def insideBoard(i, j):
        return 0 <= i < 8 and 0 <= j < 8

    def _prepareMovelist(self, cb, i, j):
        method = lambda x, y: cb.isWhitePiece(self.toSquare(x, y)) if cb.white_to_move \
            else cb.isBlackPiece(self.toSquare(x, y))
        rev_method = lambda x, y: cb.isBlackPiece(self.toSquare(x, y)) if cb.white_to_move \
            else cb.isWhitePiece(self.toSquare(x, y))
        if not method(i, j):
            return []
        piece = Piece(abs(cb[self.toSquare(i, j)].value))
        dirs = [(x, y) for x in (-1, 0, 1) for y in (-1, 0, 1) if (x or y)]
        ortho_dirs = [(x, y) for (x, y) in dirs if not (x and y)]
        diag_dirs = [(x, y) for (x, y) in dirs if (x and y)]
        knight_dirs = [(1, 2), (1, -2), (-1, 2), (-1, -2), (2, 1), (2, -1), (-2, 1), (-2, -1)]
        if piece == Piece.WhiteKing:
            # generate king moves
            for move in [(i + x, j + y) for (x, y) in dirs
                        if self.insideBoard(i + x, j + y) and not method(i + x, j + y)]:
                yield move

        if piece == Piece.WhiteQueen:
            # generate queen moves
            for d in dirs:
                for x in range(1, 8):
                    s = (i + d[0] * x, j + d[1] * x)
                    if not self.insideBoard(*s) or method(*s):
                        break
                    if rev_method(*s):
                        yield s
                        break
                    yield s

        if piece == Piece.WhiteBishop:
            # generate bishop moves
            for d in diag_dirs:
                for x in range(1, 8):
                    s = (i + d[0] * x, j + d[1] * x)
                    if not self.insideBoard(*s) or method(*s):
                        break
                    if rev_method(*s):
                        yield s
                        break
                    yield s

        if piece == Piece.WhiteKnight:
            # generate knight moves
            for move in [(i + x, j + y) for (x, y) in knight_dirs
                        if self.insideBoard(i + x, j + y) and not method(i + x, j + y)]:
                yield move

        if piece == Piece.WhiteRook:
            # generate rook moves
            for d in ortho_dirs:
                for x in range(1, 8):
                    s = (i + d[0] * x, j + d[1] * x)
                    if not self.insideBoard(*s) or method(*s):
                        break
                    if rev_method(*s):
                        yield s
                        break
                    yield s

        if piece == Piece.WhitePawn:
            # generate pawn moves
            y_dir = (-1, 1)[cb.white_to_move]
            if cb[self.toSquare(i, j + y_dir)] == Piece.Empty:
                if j + y_dir not in (0, 7):
                    yield (i, j + y_dir)  # 1 square move
                else:
                    for p in range(4):
                        yield (i, j + y_dir, p)  # Pawn promotion
            if j == 1 and y_dir == 1 or j == 6 and y_dir == -1:
                if cb[self.toSquare(i, j + y_dir)] == Piece.Empty and \
                                cb[self.toSquare(i, j + 2 * y_dir)] == Piece.Empty:
                    yield (i, j + 2 * y_dir)  # 2 square move
            for s in (-1, 1):
                if self.insideBoard(i + s, j + y_dir) and (rev_method(i + s, j + y_dir)
                                                           or self.toSquare(i + s, j + y_dir) == cb.en_passant):
                    if j + y_dir not in (0, 7):
                        yield (i + s, j + y_dir)
                    else:
                        for p in range(4):
                            yield (i + s, j + y_dir, p)  # Pawn promotion

    def generateMoves(self, cb, legal_only=True):
        for i in range(8):
            for j in range(8):
                movelist = self._prepareMovelist(cb, i, j)
                for m in movelist:
                    if len(m) == 2:
                        mx = self.toSquare(i, j) + self.toSquare(*m)
                        if not legal_only or self.isMoveLegal(cb, mx):
                            yield mx
                    else:
                        mx = self.toSquare(i, j) + self.toSquare(m[0], m[1]) + "qrbn"[m[2]]
                        if not legal_only or self.isMoveLegal(cb, mx):
                            yield mx
        if legal_only and not self.isKingChecked(cb):
            if cb.white_to_move and "K" in cb.castling and cb["f1"] == cb["g1"] == Piece.Empty and \
                    self.isMoveLegal(cb, "e1g1"):
                yield "e1g1"
            if cb.white_to_move and "Q" in cb.castling and cb["d1"] == cb["c1"] == cb["b1"] == Piece.Empty and \
                    self.isMoveLegal(cb, "e1c1"):
                yield "e1c1"
            if not cb.white_to_move and "k" in cb.castling and cb["f8"] == cb["g8"] == Piece.Empty and \
                    self.isMoveLegal(cb, "e8g8"):
                yield "e8g8"
            if not cb.white_to_move and "q" in cb.castling and cb["d8"] == cb["c8"] == cb["b8"] == Piece.Empty and \
                    self.isMoveLegal(cb, "e8c8"):
                yield "e8c8"

    def isMoveLegal(self, cb, move):
        after_move = deepcopy(cb)
        after_move.move(move)
        king = after_move.findKing(not after_move.white_to_move)
        # Check if castling is legal
        cmoves = (move == "e1g1" and king == "g1",
                  move == "e1c1" and king == "c1",
                  move == "e8g8" and king == "g8",
                  move == "e8c8" and king == "c8")
        sq = ("f1", "d1", "f8", "d8")
        castle = any(cmoves)
        if castle:
            sq = sq[cmoves.index(True)]
        else:
            del sq
        del cmoves
        opponent_moves = self.generateMoves(after_move, False)
        for om in opponent_moves:
            if om[2:4] == king or (castle and om[2:4] == sq):
                return False
        return True

    def isKingChecked(self, cb):
        cbc = deepcopy(cb)
        king = cbc.findKing(cbc.white_to_move)
        cbc.white_to_move = not cbc.white_to_move
        opponent_moves = self.generateMoves(cbc, False)
        del cbc
        for om in opponent_moves:
            if om[2:4] == king:
                return True
        return False

    def isCheckmate(self, cb):
        return not next(self.generateMoves(cb, True), "") and self.isKingChecked(cb)

    def isStalemate(self, cb):
        return not next(self.generateMoves(cb, True), "") and not self.isKingChecked(cb)

    def evaluate(self, cb):
        values = {Piece.WhiteKing: +20000, Piece.WhiteQueen: +900, Piece.WhiteRook: +480,
                  Piece.WhiteBishop: +330, Piece.WhiteKnight: +320, Piece.WhitePawn: +100,
                  Piece.BlackKing: -20000, Piece.BlackQueen: -900, Piece.BlackRook: -480,
                  Piece.BlackBishop: -330, Piece.BlackKnight: -320, Piece.BlackPawn: -100,
                  Piece.Empty: 0}
        wpawns = (0, 0, 0, 0, 0, 0, 0, 0,
                  50, 50, 50, 50, 50, 50, 50, 50,
                  10, 10, 20, 30, 30, 20, 10, 10,
                  5, 5, 10, 25, 25, 10, 5, 5,
                  0, 0, 0, 20, 20, 0, 0, 0,
                  5, -5, -10, 0, 0, -10, -5, 5,
                  5, 10, 10, -20, -20, 10, 10, 5,
                  0, 0, 0, 0, 0, 0, 0, 0)

        wknights = (-50, -40, -30, -30, -30, -30, -40, -50,
                    -40, -20, 0, 0, 0, 0, -20, -40,
                    -30, 0, 10, 15, 15, 10, 0, -30,
                    -30, 5, 15, 20, 20, 15, 5, -30,
                    -30, 0, 15, 20, 20, 15, 0, -30,
                    -30, 5, 10, 15, 15, 10, 5, -30,
                    -40, -20, 0, 5, 5, 0, -20, -40,
                    -50, -40, -30, -30, -30, -30, -40, -50)

        wbishops = (-20, -10, -10, -10, -10, -10, -10, -20,
                    -10, 0, 0, 0, 0, 0, 0, -10,
                    -10, 0, 5, 10, 10, 5, 0, -10,
                    -10, 5, 5, 10, 10, 5, 5, -10,
                    -10, 0, 10, 10, 10, 10, 0, -10,
                    -10, 10, 10, 10, 10, 10, 10, -10,
                    -10, 5, 0, 0, 0, 0, 5, -10,
                    -20, -10, -10, -10, -10, -10, -10, -20)

        wrooks = (0, 0, 0, 0, 0, 0, 0, 0,
                  5, 10, 10, 10, 10, 10, 10, 5,
                  -5, 0, 0, 0, 0, 0, 0, -5,
                  -5, 0, 0, 0, 0, 0, 0, -5,
                  -5, 0, 0, 0, 0, 0, 0, -5,
                  -5, 0, 0, 0, 0, 0, 0, -5,
                  -5, 0, 0, 0, 0, 0, 0, -5,
                  0, 0, 0, 5, 5, 0, 0, 0)

        wqueens = (-20, -10, -10, -5, -5, -10, -10, -20,
                   -10, 0, 0, 0, 0, 0, 0, -10,
                   -10, 0, 5, 5, 5, 5, 0, -10,
                   -5, 0, 5, 5, 5, 5, 0, -5,
                   0, 0, 5, 5, 5, 5, 0, -5,
                   -10, 5, 5, 5, 5, 5, 0, -10,
                   -10, 0, 5, 0, 0, 0, 0, -10,
                   -20, -10, -10, -5, -5, -10, -10, -20)

        wkings = (-50, -40, -30, -20, -20, -30, -40, -50,
                  -30, -20, -10, 0, 0, -10, -20, -30,
                  -30, -10, 20, 30, 30, 20, -10, -30,
                  -30, -10, 30, 40, 40, 30, -10, -30,
                  -30, -10, 30, 40, 40, 30, -10, -30,
                  -30, -10, 20, 30, 30, 20, -10, -30,
                  -30, -30, 0, 0, 0, 0, -30, -30,
                  -50, -30, -30, -30, -30, -30, -30, -50)

        bpawns = tuple(-wpawns[i % 8 + (7 - i // 8) * 8] for i in range(64))
        bknights = tuple(-wknights[i % 8 + (7 - i // 8) * 8] for i in range(64))
        bbishops = tuple(-wbishops[i % 8 + (7 - i // 8) * 8] for i in range(64))
        brooks = tuple(-wrooks[i % 8 + (7 - i // 8) * 8] for i in range(64))
        bqueens = tuple(-wqueens[i % 8 + (7 - i // 8) * 8] for i in range(64))
        bkings = tuple(-wkings[i % 8 + (7 - i // 8) * 8] for i in range(64))

        pst = {Piece.WhiteKing: wkings, Piece.WhiteQueen: wqueens, Piece.WhiteBishop: wbishops,
               Piece.WhiteKnight: wknights, Piece.WhiteRook: wrooks, Piece.WhitePawn: wpawns,
               Piece.BlackKing: bkings, Piece.BlackQueen: bqueens, Piece.BlackBishop: bbishops,
               Piece.BlackKnight: bknights, Piece.BlackRook: brooks, Piece.BlackPawn: bpawns}

        e = 0
        for i in range(8):
            for j in range(8):
                p = cb.board[i][j]
                e += values[p]
                if p in pst:
                    e += pst[p][(7 - i) * 8 + j]
        e *= (-1, 1)[cb.white_to_move]
        e += 10 * len(self.generateMoves(cb))
        return e

    def generateFullTree(self, cb, depth):
        cbc = deepcopy(cb)
        if depth <= 0:
            return [cbc, []]
        g = self.generateMoves(cb)
        r = [cbc, []]
        for i, m in enumerate(g):
            cbm = deepcopy(cb)
            cbm.move(m)
            print(depth, m)
            r[1].append(self.generateFullTree(cbm, depth - 1))
        return r

    def gftTimed(self, cb, depth):
        start = time.clock()
        r = self.generateFullTree(cb, depth)
        end = time.clock()
        print(end - start)
        return r

    def generateSolution(self, cb, mate_in):
        if mate_in == 1:
            pass
        elif mate_in == 2:
            return self.generateSolution_mate_in_2(cb)
        else:
            pass

    def generateSolution_mate_in_2(self, cb):
        sol = []
        ans = {}
        for m1 in self.generateMoves(cb):
            print("Checking 1.", m1)
            cbm1 = deepcopy(cb)
            cbm1.move(m1)
            for m2 in self.generateMoves(cbm1):
                s = 0
                cbm2 = deepcopy(cbm1)
                cbm2.move(m2)
                # print("Checking 1....", m2)
                for m3 in self.generateMoves(cbm2):
                    cbm3 = deepcopy(cbm2)
                    cbm3.move(m3)
                    # print("Checking 2.", m3)
                    if self.isCheckmate(cbm3):
                        s += 1
                        break
                if not s:
                    ans[m1] = m2
                    break
            if m1 not in ans:
                print(m1, " is the solution")
                sol.append(m1)
        return sol, ans

    def gsmi2TimedTest(self, cb):
        start = time.clock()
        r = self.generateSolution_mate_in_2(cb)
        end = time.clock()
        c = end - start
        print(c)
        return r, c

    def gsmi2Timed(self, cb):
        return self.gsmi2TimedTest(cb)[0]
