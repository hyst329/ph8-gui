from chessboard import Chessboard, Piece
from movegen import MoveGenerator
import sqlite3
import random
import sys

__author__ = 'hyst329'

import wx


class PawnPromotionDialog(wx.Dialog):
    def __init__(self, *args, **kw):
        super(PawnPromotionDialog, self).__init__(*args, **kw)
        vbox = wx.BoxSizer(wx.VERTICAL)
        qbut = wx.Button(self, label="Queen")
        vbox.Add(qbut)
        rbut = wx.Button(self, label="Rook")
        vbox.Add(rbut)
        bbut = wx.Button(self, label="Bishop")
        vbox.Add(bbut)
        nbut = wx.Button(self, label="Knight")
        vbox.Add(nbut)
        self.SetSizer(vbox)
        self.SetSize((100, 150))

        qbut.Bind(wx.EVT_BUTTON, lambda e: self.OnClose(e, 0))
        rbut.Bind(wx.EVT_BUTTON, lambda e: self.OnClose(e, 1))
        bbut.Bind(wx.EVT_BUTTON, lambda e: self.OnClose(e, 2))
        nbut.Bind(wx.EVT_BUTTON, lambda e: self.OnClose(e, 3))

    def OnClose(self, e, x):
        self.SetReturnCode(x)
        self.Destroy()


class MainFrame(wx.Frame):
    def __init__(self, parent, title):
        super(MainFrame, self).__init__(parent, title=title,
                                        size=(650, 458))

        self.panel = wx.Panel(self, size=(0, 0))
        self.moves = wx.TextCtrl(self, pos=(450, 50), size=(160, 300), style=wx.TE_MULTILINE | wx.TE_READONLY)
        self.moves.SetFont(wx.Font(8, wx.FONTFAMILY_TELETYPE,
                                   wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False))
        self.text = wx.StaticText(self, pos=(450, 20), label="0", size=(160, 20))
        self.board = Chessboard()
        self.puzzle_mode = True
        self.con = sqlite3.connect('pdb.sqlite')
        p = list(self.con.cursor().execute("SELECT * FROM PUZ ORDER BY RANDOM() LIMIT 1"))[0]
        self.fen = p[1]
        self.max_mate = p[2]
        wx.MessageBox("Position #%s\n%s\nMate in %s" % (p[0], p[3], p[2]), "Info")
        self.board.parseFEN(self.fen)
        self.gen = MoveGenerator()
        if self.max_mate == 2:
            t = self.gen.gsmi2Timed(self.board)
            print(t)
            self.sol, self.ans = t

        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_MOTION, self.OnMouseMove)
        self.Bind(wx.EVT_LEFT_DOWN, self.OnLeftDown)
        self.Bind(wx.EVT_LEFT_UP, self.OnLeftUp)

        self.startpos = (0, 0)
        self.movingpiece = (0, 0)
        self.delta = (0, 0)

        self.frame_count = 0

        self.buffer = wx.Bitmap(*self.ClientSize)

        self.Centre()
        self.Layout()
        self.Show()

    def OnPaint(self, e):
        dc = wx.BufferedPaintDC(self, self.buffer)
        dc.Clear()
        is_checked = self.gen.isKingChecked(self.board)
        king = Piece.WhiteKing if self.board.white_to_move else Piece.BlackKing
        for i in range(8):
            for j in range(8):
                piece = self.board[chr(ord('a') + i) + chr(ord('8') - j)]
                if is_checked and piece == king:
                    dc.SetBrush(wx.Brush("#604000" if (i + j) % 2 else "#C0A000"))
                else:
                    dc.SetBrush(wx.Brush("#303030" if (i + j) % 2 else "#D0D0D0"))
                dc.DrawRectangle(10 + i * 50, 10 + j * 50, 50, 50)
                offset = self.delta if (i == self.movingpiece[0] - 1 and
                                        j == 8 - self.movingpiece[1]) else (0, 0)
                if piece == Piece.Empty:
                    continue
                elif piece == Piece.WhiteKing:
                    dc.DrawBitmap(wx.Bitmap("img/wking.png"), 13 + i * 50 + offset[0], 13 + j * 50 + offset[1])
                elif piece == Piece.WhiteQueen:
                    dc.DrawBitmap(wx.Bitmap("img/wqueen.png"), 13 + i * 50 + offset[0], 13 + j * 50 + offset[1])
                elif piece == Piece.WhiteBishop:
                    dc.DrawBitmap(wx.Bitmap("img/wbishop.png"), 13 + i * 50 + offset[0], 13 + j * 50 + offset[1])
                elif piece == Piece.WhiteKnight:
                    dc.DrawBitmap(wx.Bitmap("img/wknight.png"), 13 + i * 50 + offset[0], 13 + j * 50 + offset[1])
                elif piece == Piece.WhiteRook:
                    dc.DrawBitmap(wx.Bitmap("img/wrook.png"), 13 + i * 50 + offset[0], 13 + j * 50 + offset[1])
                elif piece == Piece.WhitePawn:
                    dc.DrawBitmap(wx.Bitmap("img/wpawn.png"), 13 + i * 50 + offset[0], 13 + j * 50 + offset[1])
                elif piece == Piece.BlackKing:
                    dc.DrawBitmap(wx.Bitmap("img/bking.png"), 13 + i * 50 + offset[0], 13 + j * 50 + offset[1])
                elif piece == Piece.BlackQueen:
                    dc.DrawBitmap(wx.Bitmap("img/bqueen.png"), 13 + i * 50 + offset[0], 13 + j * 50 + offset[1])
                elif piece == Piece.BlackBishop:
                    dc.DrawBitmap(wx.Bitmap("img/bbishop.png"), 13 + i * 50 + offset[0], 13 + j * 50 + offset[1])
                elif piece == Piece.BlackKnight:
                    dc.DrawBitmap(wx.Bitmap("img/bknight.png"), 13 + i * 50 + offset[0], 13 + j * 50 + offset[1])
                elif piece == Piece.BlackRook:
                    dc.DrawBitmap(wx.Bitmap("img/brook.png"), 13 + i * 50 + offset[0], 13 + j * 50 + offset[1])
                elif piece == Piece.BlackPawn:
                    dc.DrawBitmap(wx.Bitmap("img/bpawn.png"), 13 + i * 50 + offset[0], 13 + j * 50 + offset[1])
                    # self.text.SetLabel(str(self.gen.evaluate(self.board)))

    def OnMouseMove(self, e):
        self.frame_count += 1
        if e.Dragging() and e.LeftIsDown():
            x, y = e.GetPosition()
            if 10 < x < 410 and 10 < y < 410:
                i = 1 + (x - 10) // 50
                j = 8 - (y - 10) // 50
            else:
                return
            if self.movingpiece != (0, 0):
                olddelta = self.delta
                self.delta = x - self.startpos[0], y - self.startpos[1]
                # if abs(olddelta[0] - self.delta[0]) > 4 and abs(olddelta[1] - self.delta[1]) > 4:
                # self.Refresh()

    def OnLeftDown(self, e):
        x, y = e.GetPosition()
        if 10 < x < 410 and 10 < y < 410:
            self.startpos = x, y
            i = 1 + (x - 10) // 50
            j = 8 - (y - 10) // 50
            if self.board[chr(i - 1 + ord('a')) + chr(j - 1 + ord('1'))]:
                self.movingpiece = i, j

    def DoMove(self, m):
        p = abs(self.board[m[0:2]].value)
        move_str = m[0:2] + "-" + m[2:4]
        move_str = " KQBNR "[p] + move_str
        if move_str in ("Ke1-g1", "Ke8-g8"):
            move_str = " 0-0  "
        if move_str in ("Ke1-c1", "Ke8-c8"):
            move_str = " 0-0-0"
        cap = self.board.move(m)
        if cap:
            move_str = move_str.replace("-", ":")
        if self.gen.isKingChecked(self.board):
            move_str += "+ "
        elif len(m) == 5:
            move_str += m[4].capitalize() + " "
        else:
            move_str += "  "
        if not self.board.white_to_move:
            move_str = "%2d" % self.board.move_number + ". " + move_str
        elif self.board.move_number == 0:
            move_str = "%2d" % self.board.move_number + "...      " + move_str + "\n"
        else:
            move_str += "\n"
        checkmate = self.gen.isCheckmate(self.board)
        stalemate = self.gen.isCheckmate(self.board)
        if checkmate or stalemate:
            if checkmate:
                move_str = move_str.replace("+", "\u2715")
                self.moves.SetValue(self.moves.GetValue() + move_str)
            result = "Checkmate" if checkmate else "Stalemate"
            winner = ("Black wins" if self.board.white_to_move else "White wins") if checkmate else "Draw"
            restart = wx.MessageBox("%s (%s). Restart?\nPress Yes to restart the same position, and No to"
                                    " select another" % (result, winner), "Game over",
                                    wx.ICON_INFORMATION | wx.YES_NO | wx.CANCEL)
            if restart == wx.YES:
                self.moves.Clear()
                self.board = Chessboard()
                self.board.parseFEN(self.fen)
                self.Refresh()
                return True
            elif restart == wx.NO:
                p = list(self.con.cursor().execute("SELECT * FROM PUZ ORDER BY RANDOM() LIMIT 1"))[0]
                self.fen = p[1]
                self.max_mate = p[2]
                wx.MessageBox("Position #%s\n%s\nMate in %s" % (p[0], p[3], p[2]), "Info")
                self.moves.Clear()
                self.board = Chessboard()
                self.board.parseFEN(self.fen)
                if self.max_mate == 2:
                    t = self.gen.gsmi2Timed(self.board)
                    print(t)
                    self.sol, self.ans = t
                return True
            else:
                return True
            return False

        else:
            self.moves.SetValue(self.moves.GetValue() + move_str)
            if self.puzzle_mode and self.max_mate == self.board.move_number and not checkmate:
                restart = wx.MessageBox("Sorry, you couldn't checkmate Black in %s moves. Try again" % self.max_mate,
                                        "Game over", wx.ICON_INFORMATION | wx.YES_NO | wx.CANCEL)
                if restart == wx.YES:
                    self.moves.Clear()
                    self.board = Chessboard()
                    self.board.parseFEN(self.fen)
                    self.Refresh()
                    return True
                elif restart == wx.NO:
                    p = list(self.con.cursor().execute("SELECT * FROM PUZ ORDER BY RANDOM() LIMIT 1"))[0]
                    self.fen = p[1]
                    self.max_mate = p[2]
                    wx.MessageBox("Position #%s\n%s\nMate in %s" % (p[0], p[3], p[2]), "Info")
                    self.moves.Clear()
                    self.board = Chessboard()
                    self.board.parseFEN(self.fen)
                    if self.max_mate == 2:
                        t = self.gen.gsmi2Timed(self.board)
                        print(t)
                        self.sol, self.ans = t
                        return True
                else:
                    return True
            return False

    def OnLeftUp(self, e):
        if self.movingpiece != (0, 0):
            x, y = self.startpos[0] + self.delta[0], self.startpos[1] + self.delta[1]
            if 10 < x < 410 and 10 < y < 410:
                i = 1 + (x - 10) // 50
                j = 8 - (y - 10) // 50
                m = MoveGenerator.toSquare(self.movingpiece[0] - 1, self.movingpiece[1] - 1) \
                    + MoveGenerator.toSquare(i - 1, j - 1)
                g = list(self.gen.generateMoves(self.board))
                if m in [x[0:4] for x in g]:
                    if m not in g:
                        d = PawnPromotionDialog(self, title="Choose Piece")
                        res = d.ShowModal()
                        m += "qrbn"[res]
                    if self.DoMove(m):
                        return
                    if m in self.ans:
                        self.DoMove(self.ans[m])
                    else:
                        self.DoMove(random.choice(list(self.gen.generateMoves(self.board))))




            self.startpos = (0, 0)
            self.movingpiece = (0, 0)
            self.Refresh()


if __name__ == '__main__':
    argc = len(sys.argv)
    if argc == 1:
        app = wx.App()
        MainFrame(None, 'ph8 GUI')
        app.MainLoop()
    elif argc == 2 and sys.argv[1] == "test":
        f = open("test.log", "w")
        print("*** Running in test mode ***", file=f)
        con = sqlite3.connect('pdb.sqlite')
        n = list(con.cursor().execute("SELECT COUNT(*) FROM PUZ"))[0][0]
        gen = MoveGenerator()
        for i in range(1, n + 1):
            print("Trying position %d..." % i, file=f)
            p = list(con.cursor().execute("SELECT * FROM PUZ WHERE ID = %d" % i))[0]
            nmax = 10
            t = 0
            for i in range(nmax):         
                board = Chessboard()
                board.parseFEN(p[1])
                if p[2] == 2:
                    time = gen.gsmi2TimedTest(board)[1]
                    t += time
            print("AVERAGE TIME OF %d attempts: %f s" % (nmax, t / nmax), file=f)
        f.close()